from akagami.application import Application as BaseApp

class App(BaseApp):
    def ping(self):
        print('Ping!')

app = App()
app.load('sandbox.json')
app.run()
