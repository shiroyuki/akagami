import codecs
import json
import Tkinter as T

class Application(object):
    def __init__(self):
        self.root = None

    def load(self, config_path):
        self.root = T.Tk()

        with codecs.open(config_path) as f:
            config = json.load(f)

        self.append_from_config(config, self.root)

    def append_from_config(self, children, parent):
        if not parent:
            raise RuntimeError('The parent element is not defined.')

        for pointer in children:
            class_name    = pointer['class']
            element_class = T.__getattribute__(class_name)
            event_handler = None
            child_configs = []

            moptions = pointer['attrs'] if 'attrs' in pointer else {}
            soptions = pointer['style'] if 'style' in pointer else {}

            del pointer['class']

            if 'bind' in pointer:
                moptions['command'] = eval(pointer['bind'])

            if 'contains' in pointer:
                child_configs.extend(pointer['contains'])

            for k in soptions:
                soptions[k] = T.__getattribute__(soptions[k].upper())

            pointer_element = element_class(parent, **moptions)
            pointer_element.pack(**soptions)

            if child_configs:
                self.append_from_config(
                    child_configs,
                    pointer_element
                )

        return parent

    def run(self):
        self.root.mainloop()
        try:
            self.root.destroy()
        except Exception as e:
            print(e) # Bypass the error
